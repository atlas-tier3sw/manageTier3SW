# This is an example file to specify a list of tool versions to install.
# The syntax is tool:version where version can be a specific version tag
#  or a shortcut (eg previous, default, testing)
# Note that dependencies are not automatically installed.

# rucio versions to install
rucio:previous-SL7
rucio:default-SL7
rucio:testing-SL7
rucio:1.23.10.patch1 # install specific version 

# panda versions to install; panda does not have a OS version specific tag yet
#panda:previous
#panda:default
#panda:testing

# grid middlewarde to install
emi:previous-SL7
emi:default-SL7
emi:testing-SL7

# xrootd to install
# eg as a dependency for rucio
xrootd:previous-SL7
xrootd:default-SL7
xrootd:testing-SL7

# python to install
# eg as a dependency for rucio wrappers using python3
python:pilot-default-SL7
python:pilot-testing-SL7
python:centos7-3.9

# logstash
logstash:default-SL7


