[[_TOC_]]
# manageTier3SW

This package will contain various utilities to manage software at a Tier3.

It uses the [ATLASLocalRootBase package](https://twiki.atlas-canada.ca/bin/view/AtlasCanada/ATLASLocalRootBase2) and will install it.

## MacOS 12.3 users with local ALRB installations:
  Apple quietly removed python so the script is broken.  To fix it, do once:
```
cd ~/userSupport/manageTier3SW
git fetch origin
git reset --hard origin/master
updateManageTier3SW.sh
```

## Installation:

  Login to your ATLAS administrator account.
```
  git clone https://gitlab.cern.ch/atlas-tier3sw/manageTier3SW.git ~/userSupport/manageTier3SW
  cd ~/userSupport/manageTier3SW
  ./updateManageTier3SW.sh -a <some installation dir>
```

You do not need the -a option if you are updating and have already defined 
ATLAS_LOCAL_ROOT_BASE.
	
## Usage:

  Login to your ATLAS administrator account.

  Ensure you have ATLAS_LOCAL_ROOT_BASE defined 

  (hint: in your ATLAS administrator account's ~/.bashrc file, add something like 

```
  export ATLAS_LOCAL_ROOT_BASE=<some installation dir>/ATLASLocalRootBase
  alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
```

  )


## Updating:
You can run updateManageTier3SW.sh as a cron job or manually.  Just check that ATLAS_LOCAL_ROOT_BASE is defined.

## Instructions for HPC sites which need minimal tools for pilots:
Some HPC sites may not have cvmfs and need only a subset of the installed tools.  This can be easily done by specifying the option --adcRH.  For example, to install only RHEL7, RHEL8 and RHEL9 tools needed for pilots (specify one or more OS versions needed here), do:
```
./updateManageTier3SW.sh --adcRH=7,8,9 -a <some installation dir>
```

and update in a cron job with the same adcRH option and arguments as above; eg to update the tools and the grid certificates::
```
export ATLAS_LOCAL_ROOT_BASE=<installHome>/ATLASLocalRootBase; updateManageTier3SW.sh --adcRH=7,8,9

export ATLAS_LOCAL_ROOT_BASE=<installHome>/ATLASLocalRootBase; $ATLAS_LOCAL_ROOT_BASE/utilities/fetchCRL-emi.sh

```

This will install a significantly smaller number of tools.  Instead of 26GB required for the current full installation (on 30 Mar 2023), it will require:
```
RHEL7: 1.3G
RHEL8: 1.5G
RHEL9: 1.2G
```

## Instructions for an ALRB only installation:
This is useful, for example for MacOS, when one wants ALRB only to launch containers.
```
export ATLAS_LOCAL_ROOT_BASE=$HOME/work/minimal/ATLASLocalRootBase
updateManageTier3SW.sh -i "none"  # update this daily !
```

## Instructions for Limited Local Installation:
One can also install a local version of ATLASLocalRootBase with selected tools and versions.

In this example, we have an installVersions.txt file that lists which tools and versions to install.  The local ATLASLocalRootBase installation will be in a directory (installHome).

example of (installHome)/installVersions.txt file:
```
# This is an example file to specify a list of tool versions to install.
# The syntax is tool:version where version can be a specific version tag
#  or a shortcut (eg previous, default, testing)
# Note that dependencies are not automatically installed.

# rucio versions to install
rucio:previous-SL7
rucio:default-SL7
rucio:testing-SL7

# grid middlewarde to install
emi:previous-SL7
emi:default-SL7
emi:testing-SL7

# xrootd to install
# eg as a dependency for rucio
xrootd:previous-SL7
xrootd:default-SL7
xrootd:testing-SL7

# python to install
# eg as a dependency for rucio wrappers using python3
python:pilot-default-SL7
python:pilot-testing-SL7

# davix
davix:default-SL7
davix:testing-SL7 
davix:previous-SL7 

# asetup
asetup:default
asetup:testing
asetup:previous

# prmon
prmon:default
prmon:testing
prmon:previous
```

Install as:

```
updateManageTier3SW.sh  -i  rucio,emi,xrootd,python,davix,asetup,prmon -f <installHome>/installVersions.txt -a <installHome> 
```

and update in a cron job with this commnd:
```
export ATLAS_LOCAL_ROOT_BASE=<installHome>/ATLASLocalRootBase; updateManageTier3SW.sh -i rucio,emi,xrootd,python,davix,asetup,prmon -f <installHome>/installVersions.txt
```
